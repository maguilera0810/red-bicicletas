var Bicicleta = require('../../models/bicicleta')
var request = require('request')
var server = require('../../bin/www')


beforeEach(()=>{
    Bicicleta.allBicis = []
})
describe('Bicicleta API', () => {
    describe('GET BICILETAS /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1, 'rojo', 'urbana', [-3.321770, -79.811599]);
            Bicicleta.add(a)
            request.get('http://localhost:3000/api/bicicletas', function (err, res, body) {
                expect(res.statusCode).toBe(200)

            })
        })
    })
    describe('POST BICILETAS /create', () => {
        it('Status 200', (done) => {
            var aBici = '{ "id":10,"color":"morado","modelo":"urbana","lat":-34,"lng":-54 }';
            request.post({
                headers:  {'content-type': 'application/json'},
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function (err, res, body) {
                expect(res.statusCode).toBe(200)
                expect(Bicicleta.findById(10).color).toBe('morado')
                done()

            })
        })
    })

    describe('POST BICILETAS /update', () => {
        it('Status 200', (done) => {
            var a = new Bicicleta(1, 'rojo', 'urbana', [-3.321770, -79.811599]);
            Bicicleta.add(a)
            var aBici = '{ "id":34,"color":"morado","modelo":"urbana","lat":-34,"lng":-54 }';
            request.post({
                headers:  {'content-type': 'application/json'},
                url: `http://localhost:3000/api/bicicletas/update/${a.id}`,
                body: aBici
            }, function (err, res, body) {
                expect(res.statusCode).toBe(200)
                expect(Bicicleta.findById(34).color).toBe('morado')
                done()

            })
        })
    })
    describe('POST BICILETAS /delete', () => {
        it('Status 200', (done) => {
            var a = new Bicicleta(1, 'rojo', 'urbana', [-3.321770, -79.811599]);
            Bicicleta.add(a)
            var aBici = '{ "id":1}';
            request.post({
                headers:  {'content-type': 'application/json'},
                url: `http://localhost:3000/api/bicicletas/delete`,
                body: aBici
            }, function (err, res, body) {
                expect(res.statusCode).toBe(200)
                expect(Bicicleta.allBicis.length).toBe(0);
                done()

            })
        })
    })
})

