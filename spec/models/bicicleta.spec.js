var Bicicleta = require('../../models/bicicleta')


beforeEach(() => {
    Bicicleta.allBicis = []
});
describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
    })

});

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
        var a = new Bicicleta(1, 'rojo', 'urbana', [-3.321770, -79.811599]);
        Bicicleta.add(a)
        expect(Bicicleta.allBicis.length).toBe(1)
        expect(Bicicleta.allBicis[0]).toBe(a)
        
    })

});
describe('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
        var a = new Bicicleta(1, 'rojo', 'urbana', [-3.321770, -79.811599]);
        var b = new Bicicleta(2, 'vere', 'montaña', [-3.321770, -79.811599]);
        Bicicleta.add(a);
        Bicicleta.add(b);
        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(a.color);
        expect(targetBici.modelo).toBe(a.modelo);
        expect(targetBici.ubicacion).toBe(a.ubicacion);
        

    })
})


describe('Bicicleta.removeById', () => {
    it('debe remover el id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
        var a = new Bicicleta(1, 'rojo', 'urbana', [-3.321770, -79.811599]);
        var b = new Bicicleta(2, 'vere', 'montaña', [-3.321770, -79.811599]);
        Bicicleta.add(a);
        Bicicleta.add(b);
        expect(Bicicleta.allBicis.length).toBe(2)
        Bicicleta.removeById(1)
        expect(Bicicleta.allBicis.length).toBe(1)
        expect(Bicicleta.allBicis[0]).toBe(b);
    })
})