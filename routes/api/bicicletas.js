const express = require('express')
const router = express.Router()
const bicicletaControllerAPI = require('../../controllers/api/bicicletaControllerAPI')
//const { bicicleta_list } = require('../../controllers/bicicleta')

router.get('/', bicicletaControllerAPI.bicicleta_list);
router.post('/create', bicicletaControllerAPI.bicicleta_create);
router.post('/delete', bicicletaControllerAPI.bicicleta_delete);
router.post('/update/:id', bicicletaControllerAPI.bicicleta_update);

module.exports = router;