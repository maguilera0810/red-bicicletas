var map = L.map('main_map').setView([-3.321968, -79.811498], 13);


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


var PulpoIcon = L.Icon.extend({
    options: {
        //shadowUrl: 'leaf-shadow.png',
        iconSize: [100, 100],
        shadowSize: [50, 64],
        iconAnchor: [22, 94],
        shadowAnchor: [4, 62],
        popupAnchor: [-3, -76]
    }
});

var pulpoIcon = new PulpoIcon({ iconUrl: '../img/pulpo2.png' });
var pulpo2Icon = new PulpoIcon({ iconUrl: '../img/pulpo.png' });


//L.marker([-3.321968, -79.811498], { icon: pulpoIcon }).addTo(map).bindPopup("I am a cat.");
//L.marker([-3.328257, -79.809610], { icon: pulpo2Icon }).addTo(map).bindPopup("I am not a cat.");


$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: function (result) {
        
        result.bicicletas.forEach(function (bici) {
            L.marker(bici.ubicacion, { icon: pulpoIcon }).addTo(map).bindPopup(`I am a cat. ${bici.modelo}`);
            
        })
    }
})