var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function () {
    return `id: ${this.id} | color: ${this.color}`
}


Bicicleta.allBicis = []
Bicicleta.add = (aBici) => {
    
    Bicicleta.allBicis.push(aBici)
}

Bicicleta.findById = function (id) {
    var aBici = Bicicleta.allBicis.find(x => x.id == id);
    if (aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${id}`)

}

Bicicleta.removeById = function (id) {
    //Bicicleta.findById(id)
    
    for (let i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == id) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

/* var a = new Bicicleta(1, 'rojo', 'urbana', [-3.321970, -79.811499]);
var b = new Bicicleta(2, 'blanca', 'urbana', [-3.328255, -79.809611]);
var c = new Bicicleta(3, 'negra', 'urbana', [-3.330520, -79.810211]);
var d = new Bicicleta(4, 'morada', 'montaña', [-3.327155, -79.809390]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
Bicicleta.add(d); */

module.exports = Bicicleta